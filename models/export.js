// BOARD FUNCTIONS
const {
    boardExists,
    createBoard
} = require("./functions/board");

// TASK FUNCTIONS
const {
    getAllTask,
    createTask,
    removeTask,
    updateTask
} = require("./functions/task");

// PERSON FUNCTIONS
const {
    getAllPeople,
    createPerson,
    removePerson,
    updatePerson,
} = require("./functions/person");

// TASKPERSON FUNCTIONS
const {
    getAllTaskPeople,
    createTaskPerson,
    removeTaskPerson
} = require("./functions/taskperson");

// PASSWORD FUNCTIONS
const {
    matchHash,
    createHash
} = require("./functions/password");

// SUBTASK FUNCTIONS
const {
    getAllSubtask,
    createSubtask,
    updateSubtask,
    removeSubtask
} = require("./functions/subtask");

module.exports = {
    // board related functions
    boardExists, createBoard,

    // task related functions
    getAllTask, createTask, removeTask, updateTask,

    // people related functions
    getAllPeople, createPerson, removePerson, updatePerson,

    // TaskPerson related functions
    getAllTaskPeople, createTaskPerson, removeTaskPerson,

    // password related functions
    matchHash, createHash,

    // subtask related functions
    getAllSubtask, createSubtask, updateSubtask, removeSubtask
}