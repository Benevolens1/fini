const { TaskPerson } = require("../model");

function getAllTaskPeople(boardId) {
    return new Promise((resolve, reject) => {
        TaskPerson.findAll({ where: { boardId: boardId } })
            .then((data) => resolve(data))
            .catch((err) => reject(err));
    });
}

function createTaskPerson(taskId, personId, boardId) {
    return new Promise((resolve, reject) => {
        TaskPerson.create({ taskId: taskId, personId: personId, boardId: boardId })
            .then(() => resolve())
            .catch((err) => reject(err));
    });
}

function removeTaskPerson(taskId, personId, boardId) {
    return new Promise((resolve, reject) => {
        TaskPerson.destroy({ where: { taskId: taskId, personId: personId, boardId: boardId } })
            .then(() => resolve())
            .catch((err) => reject(err));
    });
}

module.exports = {
    getAllTaskPeople,
    createTaskPerson,
    removeTaskPerson
}