const { Password } = require("../model");

async function matchHash(inputHash) {
    let savedPassword = await Password.findOne({ where: { hash: inputHash } });
    if (savedPassword === null) {
        return false;
    } else if (savedPassword.hash === inputHash) {
        return true;
    } else {
        return false;
    }
}

async function createHash(inputHash) {
    await Password.create({ hash: inputHash });
}

module.exports = {
    matchHash,
    createHash
}