const { Board } = require('../model');

// BOARD FUNCTIONS
async function boardExists(boardId) {
    let board = await Board.findOne({ where: { boardId: boardId } });
    let exists = board != null ? true : false;
    return exists;
}

async function createBoard(boardId) {
    let board = { boardId: boardId };
    await Board.create(board)
        .catch(err => console.log(err));
}

module.exports = {
    boardExists,
    createBoard
}