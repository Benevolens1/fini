const { Subtask } = require("../model");
const { makeRandomId } = require("./generalPurpose");

async function getAllSubtask(boardId) {
    return new Promise(async (resolve, reject) => {
        Subtask.findAll({ where: { boardId: boardId } })
            .then(subtasks => resolve(subtasks))
            .catch(err => reject(err));
    });
}

async function createSubtask(subtask, boardId) {
    return new Promise((resolve, reject) => {
        subtask.taskId = makeRandomId();
        subtask.boardId = boardId;
        Subtask.create(subtask)
            .then(() => resolve(subtask))
            .catch(err => reject(err));
    });
}

async function updateSubtask(subtask, boardId) {
    return new Promise((resolve, reject) => {
        Subtask.update(subtask, {
            where: {
                boardId: boardId,
                taskId: subtask.taskId
            }
        })
            .then(() => resolve())
            .catch(err => reject(err));
    })
}

async function removeSubtask(taskId, boardId) {
    return new Promise((resolve, reject) => {
        Subtask.destroy({ where: { taskId: taskId, boardId: boardId } })
            .then(() => resolve())
            .catch(err => reject(err));
    });
}

module.exports = {
    getAllSubtask,
    createSubtask,
    updateSubtask,
    removeSubtask
}