const { Task, Subtask } = require("../model");
const { makeRandomId } = require("./generalPurpose");

function getAllTask(boardId) {
    return new Promise(async (resolve, reject) => {
        try {
            let tasks = await Task.findAll({ where: { boardId: boardId } })
            resolve(tasks);
        } catch (err) {
            reject(err);
        }
    });
}

function createTask(task, boardId) {
    return new Promise((resolve, reject) => {
        task.taskId = makeRandomId();
        task.boardId = boardId;
        Task.create(task)
            .then(() => {
                task.people = [];
                resolve(task)
            })
            .catch((err) => reject(err));
    });
}

function removeTask(taskId, boardId) {
    return new Promise((resolve, reject) => {

        Promise.all([
            Task.destroy({
                where: {
                    taskId: taskId,
                    boardId: boardId
                }
            }),
            Subtask.destroy({
                where: {
                    parentId: taskId,
                    boardId: boardId
                }
            })
        ])
            .then(([_, _2]) => resolve())
            .catch(err => reject(err));
    });
}

function updateTask(task, boardId) {
    return new Promise((resolve, reject) => {
        Task.update(task, {
            where: {
                taskId: task.taskId,
                boardId: boardId
            }
        })
            .then(() => resolve())
            .catch((err) => reject(err));
    });
}

module.exports = {
    getAllTask,
    createTask,
    removeTask,
    updateTask
}