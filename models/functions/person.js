const { Person, TaskPerson } = require("../model");
const { makeRandomId } = require("./generalPurpose");

function getAllPeople(boardId) {
    return new Promise((resolve, reject) => {
        Person.findAll({ where: { boardId: boardId } })
            .then((data) => resolve(data))
            .catch((err) => reject(err));
    });
}

function createPerson(person, boardId) {
    return new Promise((resolve, reject) => {
        person.personId = makeRandomId();
        person.boardId = boardId;
        Person.create(person)
            .then(() => resolve(person))
            .catch((err) => reject(err));
    });
}

function removePerson(personId, boardId) {
    return new Promise(async (resolve, reject) => {
        try {
            await Person.destroy({
                where: {
                    personId: personId,
                    boardId: boardId
                }
            });

            await TaskPerson.destroy({
                where: {
                    personId: personId,
                    boardId: boardId
                }
            });

            resolve();

        } catch (e) {
            reject(e);
        }

    });
}

function updatePerson(person, boardId) {
    return new Promise((resolve, reject) => {
        Person.update(person, {
            where: {
                personId: person.personId,
                boardId: boardId
            }
        })
            .then(() => resolve())
            .catch((err) => reject(err));
    });
}

module.exports = {
    getAllPeople,
    createPerson,
    removePerson,
    updatePerson,
}