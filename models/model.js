const { Sequelize, DataTypes, Model, Op } = require('sequelize');

const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: 'database.sqlite'
});


// DB MODELS
class Board extends Model { }
Board.init({
  boardId: {
    type: DataTypes.STRING,
    allowNull: false
  }
}, {
  sequelize,
  modelName: 'Board'
});

class Task extends Model { }
Task.init({
  taskId: { // task random id
    type: DataTypes.STRING,
    allowNull: false
  },
  state: {
    type: DataTypes.STRING,
    defaultValue: 'todo'
  },
  title: {
    type: DataTypes.STRING
  },
  content: {
    type: DataTypes.TEXT
  },
  boardId: { // board random id
    type: DataTypes.STRING,
    allowNull: false
  }
}, {
  sequelize,
  modelName: 'Task'
});

class Person extends Model { }
Person.init({
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  personId: {// person random id
    type: DataTypes.STRING,
    allowNull: false
  },
  boardId: {
    type: DataTypes.STRING,
    allowNull: false
  }
}, {
  sequelize,
  modelName: 'Person'
});

class TaskPerson extends Model { }
TaskPerson.init({
  taskId: {
    type: DataTypes.STRING,
    allowNull: false
  },
  personId: {
    type: DataTypes.STRING,
    allowNull: false
  },
  boardId: {
    type: DataTypes.STRING,
    allowNull: false
  }
}, {
  sequelize,
  modelName: 'TaskPerson'
});

class Password extends Model { }
Password.init({
  hash: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true,
  }
}, {
  sequelize,
  modelName: 'Password'
});

class Subtask extends Model { }
Subtask.init({
  boardId: {
    type: DataTypes.STRING,
    allowNull: false
  },
  taskId: { // task random id
    type: DataTypes.STRING,
    allowNull: false
  },
  parentId: {
    type: DataTypes.STRING,
    allowNull: false
  },
  state: {
    type: DataTypes.STRING,
    defaultValue: 'todo'
  },
  content: {
    type: DataTypes.TEXT
  },
}, {
  sequelize,
  modelName: 'Subtask'
});

(async () => {
  await sequelize.sync();
})();

module.exports = {
  Board,
  Task,
  Person,
  TaskPerson,
  Password,
  Subtask
}