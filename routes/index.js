var express = require('express');
var router = express.Router();
var rootRoute = express.Router();

var ctrl = require('../controllers/controller');

rootRoute.get('/*', ctrl.getHomePage);

router.get('/newboard/:password', ctrl.getNewBoard);

router.get('/boardId/:boardId', ctrl.getBoardPage);

router.get('/tasks/:boardId', ctrl.getTasks);

router.get('/people/:boardId', ctrl.getPeople);

router.get('/taskpeople/:boardId', ctrl.getTaskPeople);

router.get('/subtasks/:boardId', ctrl.getSubtasks)



module.exports = {
    router: router,
    rootRoute: rootRoute
};