const { randomBytes, createHash } = require('node:crypto');
const model = require('../models/export');
var path = require('path');

function getHomePage(req, res) {
  if (process.env.NODE_ENV === "prod") {
    res.sendFile(path.join(__dirname, "/../dist/index.html"))
  }
}

function getNewBoard(req, res) {
  let password = req.params.password;
  const hash = createHash('sha256');
  hash.update(password);
  let passwordHash = hash.digest('hex');

  model.matchHash(passwordHash)
    .then(match => {
      if (match) {
        const buf = randomBytes(13);
        const boardId = buf.toString('hex');
        model.createBoard(boardId);
        res.send(boardId);
      } else {
        res.send("invalid password");
      }
    });
}

async function getBoardPage(req, res) {
  let boardId = req.params.boardId;
  if (await model.boardExists(boardId)) {
    res.sendFile(path.join(__dirname, "/../dev/index.html"))
  } else {
    res.status(404).end();
  }
}

function getTasks(req, res) {
  let boardId = req.params.boardId;
  if (model.boardExists(boardId) === false) return res.status(404).end();
  model.getAllTask(boardId)
    .then(data => res.send(data))
    .catch(err => console.log(err));
}

function getPeople(req, res) {
  let boardId = req.params.boardId;
  if (model.boardExists(boardId) === false) return res.status(404).end();
  model.getAllPeople(boardId)
    .then(people => res.send(people))
    .catch(err => console.log(err));
}

function getTaskPeople(req, res) {
  let boardId = req.params.boardId;
  if (model.boardExists(boardId) === false) return res.status(404).end();
  model.getAllTaskPeople(boardId)
    .then(taskPeople => res.send(taskPeople))
    .catch(err => console.log(err));
}

function getSubtasks(req, res) {
  let boardId = req.params.boardId;
  if (model.boardExists(boardId) === false) return res.status(404).end();
  model.getAllSubtask(boardId)
    .then(subtasks => res.send(subtasks))
    .catch(err => console.log(err));
}

module.exports = { getHomePage, getNewBoard, getBoardPage, getTasks, getPeople, getTaskPeople, getSubtasks }