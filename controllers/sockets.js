const io = require("socket.io")();
const model = require('../models/export');

io.on("connection", function (socket) {
  let join = { boardId: false };

  socket.on('boardId', async function (boardId) {
    let exists = await model.boardExists(boardId);
    if (exists === false) return;
    socket.join(boardId);
    join.boardId = boardId;
    console.log("A user connected");
  });

  socket.on('createTask', function (task) {
    if (join.boardId === false) return;
    model.createTask(task, join.boardId)
      .then((task) => io.to(join.boardId).emit('createTask', task))
      .catch(err => console.log(err));
  });

  socket.on('deleteTask', function (taskId) {
    if (join.boardId === false) return;
    model.removeTask(taskId, join.boardId)
      .then(() => io.to(join.boardId).emit('deleteTask', taskId))
      .catch(err => console.log(err));
  });

  socket.on('updateTask', function (task) {
    if (join.boardId === false) return;
    model.updateTask(task, join.boardId)
      .then(() => io.to(join.boardId).emit('updateTask', task))
      .catch((err) => console.log(err));
  });

  socket.on('createPerson', function (person) {
    if (join.boardId === false) return;
    model.createPerson(person, join.boardId)
      .then((person) => io.to(join.boardId).emit('createPerson', person))
      .catch(err => console.log(err));
  });

  socket.on('deletePerson', function (personId) {
    if (join.boardId === false) return;
    model.removePerson(personId, join.boardId)
      .then(() => io.to(join.boardId).emit('deletePerson', personId))
      .catch((err) => console.log(err));
  });

  socket.on('createTaskPerson', function (taskId, personId) {
    if (join.boardId === false) return;
    model.createTaskPerson(taskId, personId, join.boardId)
      .then(() => io.to(join.boardId).emit('createTaskPerson', taskId, personId))
      .catch((err) => console.log(err));
  });

  socket.on('deleteTaskPerson', function (taskId, personId) {
    if (join.boardId === false) return;
    model.removeTaskPerson(taskId, personId, join.boardId)
      .then(() => io.to(join.boardId).emit('deleteTaskPerson', taskId, personId))
      .catch((err) => console.log(err));
  });

  socket.on('somebodyElseModifies', function (taskId) {
    socket.broadcast.emit('somebodyElseModifies', taskId);
  });

  socket.on('somebodyElseStopsModification', function (taskId) {
    socket.broadcast.emit('somebodyElseStopsModification', taskId);
  });

  socket.on('createSubtask', function (subtask) {
    if (join.boardId === false) return;
    model.createSubtask(subtask, join.boardId)
      .then((subtask) => io.to(join.boardId).emit('createSubtask', subtask))
      .catch(err => console.log(err));
  });

  socket.on('updateSubtask', function (subtask) {
    if (join.boardId === false) return;
    model.updateSubtask(subtask, join.boardId)
      .then(() => io.to(join.boardId).emit('updateSubtask', subtask))
      .catch(err => console.log(err));
  });

  socket.on('deleteSubtask', function (taskId) {
    if (join.boardId === false) return;
    model.removeSubtask(taskId, join.boardId)
      .then(() => io.to(join.boardId).emit('deleteSubtask', taskId))
      .catch(err => console.log(err));
  });
});

module.exports = { io };