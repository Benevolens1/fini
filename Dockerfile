FROM node:18-bookworm-slim AS db-init
WORKDIR /app
ENV PASSWORD=a
COPY . .
RUN npm install
RUN node models/model.js
RUN node addPassword.js

FROM scratch AS export-stage
COPY --from=db-init /app/database.sqlite /
