const {
  createHash,
} = require('node:crypto');

const hash = createHash('sha256');

const model = require('./models/export');

hash.update(process.env.PASSWORD);
let passwordHash = hash.digest('hex')
model.createHash(passwordHash);
