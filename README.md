# fini

easy, intuitive, responsive, free (as in freedom), self-hostable kanban board web app with collaboration in real time

## Quickstart

See [fini-host](https://codeberg.org/fini-project/fini-host) to run the project.

To create a board, enter in the form the password. Click on the "Go !" button. You will be redirected to a new board. To share a board, you have to copy and paste the url, which contains your ip address or your domain name followed by a slash and the board id.  
Example : *example.org/350fc8c9801a89c1ff876a479f* or *192.168.1.42/350fc8c9801a89c1ff876a479f*

## License

This project is licensed under BSD-4-Clause.  
It uses works licensed under MIT. See below.


### React
Copyright (c) Facebook, Inc. and its affiliates.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

### express generator
Copyright (c) 2009-2013 TJ Holowaychuk <tj@vision-media.ca>  
Copyright (c) 2015-2018 Douglas Christopher Wilson <doug@somethingdoug.com>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
'Software'), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

### express js
Copyright (c) 2009-2014 TJ Holowaychuk <tj@vision-media.ca>  
Copyright (c) 2013-2014 Roman Shtylman <shtylman+expressjs@gmail.com>  
Copyright (c) 2014-2015 Douglas Christopher Wilson <doug@somethingdoug.com>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
'Software'), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

### Sequelize
Copyright (c) 2014-present Sequelize Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

### socket.io
Copyright (c) 2014-2018 Automattic <dev@cloudup.com>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
'Software'), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.
